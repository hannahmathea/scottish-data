import pandas as pd
from scipy.stats import linregress
import seaborn as sns
import matplotlib.pyplot as plt
from process_drug_data import process_drug_data


def process_deprivation_data():
    dfDep = pd.read_csv('/Users/rupert/hannah_data/deprivation/staged/deprivation_staged.csv')
    dfDep2 = dfDep.sort_values(by=['council_area']).reset_index(drop=True)
    return dfDep2


def combine_data(dfDep2, dfDrug3):
    dfCombined = dfDep2.join(dfDrug3, how='outer')
    dfCombined1 = dfCombined.rename(columns={'all_drug_related_deaths': 'Count of Drug Related Deaths', 'simd_rank': 'Deprivation Index'})
    return dfCombined1


def run_regression_graph(dfCombined1):
    colour = 'green'
    plt.figure()
    mySNS = sns.set(rc={'figure.figsize':(11.7,8.27)}, style='whitegrid')
    sns.set_context("paper", rc={"font.size":20,"axes.titlesize":50,"axes.labelsize":20})


    fig1 = sns.regplot('Deprivation Index', 'Count of Drug Related Deaths', data=dfCombined1, scatter_kws={"color": "black"}, line_kws={"color": colour}).axes.set_title(f"Linear Regression between Deprivation Index ranking and \n Drug-Related Deaths for all Local Authorities", fontsize=20)
    fig2 = fig1.get_figure()
    fig2.savefig(f"deprivation_index_vs_drug_related_deaths_regression.png")


def run_regression_values(dfCombined1):
    pass # For now...
    #     Regression 1 using scipy
    #     a = dfDrug2['all_drug_related_deaths'].values.tolist()
    #     b = dfUnem3['population_value'].values.tolist()

    #     linregress(a, b)


def main():
    dfDep2 = process_drug_data()

    dfDrug3 = process_deprivation_data()

    dfCombined1 = combine_data(dfDep2, dfDrug3)

    run_regression_graph(dfCombined1)

    run_regression_values(dfCombined1)


if __name__ == '__main__':
    main()