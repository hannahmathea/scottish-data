import pandas as pd
from scipy.stats import linregress
import seaborn as sns
import matplotlib.pyplot as plt
from process_drug_data import process_drug_data


def process_income_data():
    dfUnem = pd.read_csv('/Users/rupert/hannah_data/unemployment/tables/table_unemployment_timeseries_3_codes_and_scotland.csv')
    dfUnem2 = dfUnem[dfUnem['calendar_date'] != '2018-01-01']
    dfUnem3 = dfUnem2.sort_values(by=['la_name', 'calendar_date']).reset_index(drop=True)
    return dfUnem3


def combine_data(dfUnem3, dfDrug3):
    dfCombined = dfUnem3.join(dfDrug3, how='outer')
    dfCombined1 = dfCombined.dropna(subset = ['la_name'])
    dfCombined2 = dfCombined1.rename(columns={'all_drug_related_deaths': 'Count of Drug Related Deaths', 'unemployment_value': 'Unemployment Count'})
    return dfCombined2


def run_regression_graph(dfCombined2):
    colour = 'green'
    plt.figure()
    mySNS = sns.set(rc={'figure.figsize':(11.7,8.27)}, style='whitegrid')
    sns.set_context("paper", rc={"font.size":20,"axes.titlesize":50,"axes.labelsize":20})

    fig1 = sns.regplot('Unemployment Count', 'Count of Drug Related Deaths', data=dfCombined2, scatter_kws={"color": "black"}, line_kws={"color": colour}).axes.set_title(f"Linear Regression between Unemployment Count and \n Drug-Related Deaths for all Local Authorities", fontsize=20)
    fig2 = fig1.get_figure()
    fig2.savefig(f"unemployment_count_vs_drug_related_deaths_regression.png")
    return fig2


def run_regression_values(dfCombined1):
    pass # For now...
    #     Regression 1 using scipy
    #     a = dfDrug2['all_drug_related_deaths'].values.tolist()
    #     b = dfUnem3['population_value'].values.tolist()

    #     linregress(a, b)


def main():
    dfDrug3 = process_drug_data()
    dfUnem3 = process_unemployment_data()
    dfCombined2 = combine_data(dfUnem3, dfDrug3)
    run_regression_graph(dfCombined2)


if __name__ == '__main__':
    main()