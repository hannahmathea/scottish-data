import pandas as pd


def process_drug_data():
    dfDrug = pd.read_csv('/Users/rupert/hannah_data/drug_deaths/tables/table_drug_deaths_timeseries_3_codes_and_scotland.csv')
    dfDrug2 = dfDrug.sort_values(by=['la_name', 'calendar_date']).reset_index(drop=True)
    dfDrug3 = dfDrug2[['all_drug_related_deaths']]
    return dfDrug3


def main():
    process_drug_data()


if __name__ == '__main__':
    process_drug_data()