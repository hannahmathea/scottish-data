
-- Tables are manually loaded to the database at the moment due to complexity of changes between certain years data.

-- 2007:
DROP TABLE IF EXISTS scot_data.drug_deaths_raw_2007;
CREATE TABLE scot_data.drug_deaths_raw_2007 (
    area_name text,
    heroin_morphine_2 int,
    methadone int,
    diazepam_3 int,
    cocaine int,
    ecstasy int,
    temazepam_3 int,
    alcohol int
);

\copy scot_data.drug_deaths_raw_2007 FROM '/Users/rupert/hannah_data/drug_deaths/raw/2007.csv' WITH DELIMITER ',' CSV HEADER;



-- 2008
DROP TABLE IF EXISTS scot_data.drug_deaths_raw_2008;
CREATE TABLE scot_data.drug_deaths_raw_2008 (
    area_name text,
    heroin_morphine_2 int,
    methadone int,
    benzodiazepines int,
    benzodiazepines_diazepam int,
    benzodiazepines_temazepam int,
    cocaine int,
    ecstasy int,
    amphetamines int,
    alcohol int
);

\copy scot_data.drug_deaths_raw_2008 FROM '/Users/rupert/hannah_data/drug_deaths/raw/2008.csv' WITH DELIMITER ',' CSV HEADER;




-- 2009
DROP TABLE IF EXISTS scot_data.drug_deaths_raw_2009;
CREATE TABLE scot_data.drug_deaths_raw_2009 (
  area_name text,
  heroin_morphine_2 int,
  methadone int,
  benzodiazepines int,
  benzodiazepines_diazepam int,
  benzodiazepines_temazepam int,
  cocaine int,
  ecstasy int,
  amphetamines int,
  alcohol int
);

\copy scot_data.drug_deaths_raw_2009 FROM '/Users/rupert/hannah_data/drug_deaths/raw/2009.csv' WITH DELIMITER ',' CSV HEADER;




-- 2010
DROP TABLE IF EXISTS scot_data.drug_deaths_raw_2010;
CREATE TABLE scot_data.drug_deaths_raw_2010 (
    area_name text,
    heroin_morphine_2 int,
    methadone int,
    benzodiazepines int,
    benzodiazepines_diazepam int,
    benzodiazepines_temazepam int,
    cocaine int,
    ecstasy int,
    amphetamines int,
    alcohol int
);

\copy scot_data.drug_deaths_raw_2010 FROM '/Users/rupert/hannah_data/drug_deaths/raw/2010.csv' WITH DELIMITER ',' CSV HEADER;




-- 2011
DROP TABLE IF EXISTS scot_data.drug_deaths_raw_2011;
CREATE TABLE scot_data.drug_deaths_raw_2011 (
    area_name text,
    all_drug_related_deaths int,
    heroin_morphine_2 int,
    methadone int,
    benzodiazepines int,
    benzodiazepines_diazepam int,
    benzodiazepines_temazepam int,
    cocaine int,
    ecstasy int,
    amphetamines int,
    alcohol int
);

\copy scot_data.drug_deaths_raw_2011 FROM '/Users/rupert/hannah_data/drug_deaths/raw/2011.csv' WITH DELIMITER ',' CSV HEADER;




-- 2012
DROP TABLE IF EXISTS scot_data.drug_deaths_raw_2012;
CREATE TABLE scot_data.drug_deaths_raw_2012 (
    area_name text,
    all_drug_related_deaths int,
    heroin_morphine_2 int,
    methadone int,
    benzodiazepines int,
    benzodiazepines_diazepam int,
    benzodiazepines_temazepam int,
    cocaine int,
    ecstasy int,
    amphetamines int,
    alcohol int
);

\copy scot_data.drug_deaths_raw_2012 FROM '/Users/rupert/hannah_data/drug_deaths/raw/2012.csv' WITH DELIMITER ',' CSV HEADER;




-- 2013
DROP TABLE IF EXISTS scot_data.drug_deaths_raw_2013;
CREATE TABLE scot_data.drug_deaths_raw_2013 (
    area_name text,
    all_drug_related_deaths int,
    heroin_morphine_2 int,
    methadone int,
    benzodiazepines int,
    benzodiazepines_diazepam int,
    benzodiazepines_temazepam int,
    cocaine int,
    ecstasy int,
    amphetamines int,
    alcohol int
);

\copy scot_data.drug_deaths_raw_2013 FROM '/Users/rupert/hannah_data/drug_deaths/raw/2013.csv' WITH DELIMITER ',' CSV HEADER;




-- 2014
DROP TABLE IF EXISTS scot_data.drug_deaths_raw_2014;
CREATE TABLE scot_data.drug_deaths_raw_2014 (
    area_name text,
    all_drug_related_deaths int,
    heroin_morphine_2 int,
    methadone int,
    heroin_morphine_methadone_buprenorphine int,
    codeine_or_containing int,
    dihydro_codeine_containing int,
    any_opiate_or_opiod int,
    benzodiazepines int,
    benzodiazepines_diazepam int,
    cocaine int,
    ecstasy int,
    amphetamines int,
    alcohol int
);

\copy scot_data.drug_deaths_raw_2014 FROM '/Users/rupert/hannah_data/drug_deaths/raw/2014.csv' WITH DELIMITER ',' CSV HEADER;




-- 2015
DROP TABLE IF EXISTS scot_data.drug_deaths_raw_2015;
CREATE TABLE scot_data.drug_deaths_raw_2015 (
    area_name text,
    all_drug_related_deaths int,
    heroin_morphine_2 int,
    methadone int,
    heroin_morphine_methadone_buprenorphine int,
    codeine_or_containing int,
    dihydro_codeine_containing int,
    any_opiate_or_opiod int,
    benzodiazepines int,
    benzodiazepines_diazepam int,
    cocaine int,
    ecstasy int,
    amphetamines int,
    alcohol int
);

\copy scot_data.drug_deaths_raw_2015 FROM '/Users/rupert/hannah_data/drug_deaths/raw/2015.csv' WITH DELIMITER ',' CSV HEADER;




-- 2016
DROP TABLE IF EXISTS scot_data.drug_deaths_raw_2016;
CREATE TABLE scot_data.drug_deaths_raw_2016 (
    area_name text,
    all_drug_related_deaths int,
    heroin_morphine_2 int,
    methadone int,
    heroin_morphine_methadone_buprenorphine int,
    codeine_or_containing int,
    dihydro_codeine_containing int,
    any_opiate_or_opiod int,
    benzodiazepines int,
    benzodiazepines_diazepam int,
    cocaine int,
    ecstasy int,
    amphetamines int,
    alcohol int
);

\copy scot_data.drug_deaths_raw_2016 FROM '/Users/rupert/hannah_data/drug_deaths/raw/2016.csv' WITH DELIMITER ',' CSV HEADER;




-- 2017
DROP TABLE IF EXISTS scot_data.drug_deaths_raw_2017;
CREATE TABLE scot_data.drug_deaths_raw_2017 (
    area_name text,
    all_drug_related_deaths int,
    heroin_morphine_2 int,
    methadone int,
    heroin_morphine_methadone_buprenorphine int,
    codeine_or_containing int,
    dihydro_codeine_containing int,
    any_opiate_or_opiod int,
    benzodiazepines int,
    benzodiazepines_diazepam int,
    cocaine int,
    ecstasy int,
    amphetamines int,
    alcohol int
);

\copy scot_data.drug_deaths_raw_2017 FROM '/Users/rupert/hannah_data/drug_deaths/raw/2017.csv' WITH DELIMITER ',' CSV HEADER;



DROP TABLE IF EXISTS scot_data.drug_deaths_missing_1997_2010;
CREATE TABLE scot_data.drug_deaths_missing_1997_2010 (
    council_area text,
    value_2000 text,
    value_2001 text,
    value_2002 text,
    value_2003 text,
    value_2004 text,
    value_2005 text,
    value_2006 text,
    value_2007 text,
    value_2008 text,
    value_2009 text,
    value_2010 text,
    unknown_1 text,
    avg_1996_to_2000 text,
    avg_2006_to_2010 text,
    unknown_2 text,
    population_in_2008 text,
    average_deaths_per_1000_pop text
);

\copy scot_data.drug_deaths_missing_1997_2010 FROM '/Users/rupert/hannah_data/drug_deaths/raw/total_drug_deaths_2010_council.csv' WITH DELIMITER ',' CSV HEADER;





DROP TABLE IF EXISTS scot_data.drug_deaths_missing_1997_2010_stage;
CREATE TABLE scot_data.drug_deaths_missing_1997_2010_stage AS
    SELECT
        TRIM(council_area) AS area_name,
        value_2000::int,
        value_2001::int,
        value_2002::int,
        value_2003::int,
        value_2004::int,
        value_2005::int,
        value_2006::int,
        value_2007::int,
        value_2008::int,
        value_2009::int,
        value_2010::int
    FROM scot_data.drug_deaths_missing_1997_2010
    WHERE council_area IS NOT NULL;




DROP TABLE IF EXISTS scot_data.drug_deaths_raw_all_years;
CREATE TABLE scot_data.drug_deaths_raw_all_years AS

    SELECT *
    FROM (
    SELECT
        A.area_name,
        heroin_morphine_2 AS heroin_morphine_deaths_value,
        '2007-01-01'::date AS calendar_date,
        B.value_2007::integer AS all_drug_related_deaths
    FROM scot_data.drug_deaths_raw_2007 A
    JOIN scot_data.drug_deaths_missing_1997_2010_stage B
    ON A.area_name = B.area_name
    ) C_2007

    UNION ALL
    SELECT *
    FROM (
    SELECT
        A.area_name,
        heroin_morphine_2 AS heroin_morphine_deaths_value,
        '2008-01-01'::date AS calendar_date,
        B.value_2008 AS all_drug_related_deaths
    FROM scot_data.drug_deaths_raw_2008 A
    JOIN scot_data.drug_deaths_missing_1997_2010_stage B
    ON A.area_name = B.area_name
    ) C_2008
    UNION ALL
    SELECT *
    FROM (
    SELECT
        A.area_name,
        heroin_morphine_2 AS heroin_morphine_deaths_value,
        '2009-01-01'::date AS calendar_date,
        B.value_2009 AS all_drug_related_deaths
    FROM scot_data.drug_deaths_raw_2009 A
    JOIN scot_data.drug_deaths_missing_1997_2010_stage B
    ON A.area_name = B.area_name
    ) C_2009
    UNION ALL
    SELECT *
    FROM (
    SELECT
        A.area_name,
        heroin_morphine_2 AS heroin_morphine_deaths_value,
        '2010-01-01'::date AS calendar_date,
        B.value_2010 AS all_drug_related_deaths
    FROM scot_data.drug_deaths_raw_2010 A
    JOIN scot_data.drug_deaths_missing_1997_2010_stage B
    ON A.area_name = B.area_name
    ) C_2010
    UNION ALL
    SELECT
        area_name,
        heroin_morphine_2 AS heroin_morphine_deaths_value,
        '2011-01-01'::date AS calendar_date,
        all_drug_related_deaths
    FROM scot_data.drug_deaths_raw_2011
    UNION ALL
    SELECT
        area_name,
        heroin_morphine_2 AS heroin_morphine_deaths_value,
        '2012-01-01'::date AS calendar_date,
        all_drug_related_deaths
    FROM scot_data.drug_deaths_raw_2012
    UNION ALL
    SELECT
        area_name,
        heroin_morphine_2 AS heroin_morphine_deaths_value,
        '2013-01-01'::date AS calendar_date,
        all_drug_related_deaths
    FROM scot_data.drug_deaths_raw_2013
    UNION ALL
    SELECT
        area_name,
        heroin_morphine_2 AS heroin_morphine_deaths_value,
        '2014-01-01'::date AS calendar_date,
        all_drug_related_deaths
    FROM scot_data.drug_deaths_raw_2014
    UNION ALL
    SELECT
        area_name,
        heroin_morphine_2 AS heroin_morphine_deaths_value,
        '2015-01-01'::date AS calendar_date,
        all_drug_related_deaths
    FROM scot_data.drug_deaths_raw_2015
    UNION ALL
    SELECT
        area_name,
        heroin_morphine_2 AS heroin_morphine_deaths_value,
        '2016-01-01'::date AS calendar_date,
        all_drug_related_deaths
    FROM scot_data.drug_deaths_raw_2016
    UNION ALL
    SELECT
        area_name,
        heroin_morphine_2 AS heroin_morphine_deaths_value,
        '2017-01-01'::date AS calendar_date,
        all_drug_related_deaths
    FROM scot_data.drug_deaths_raw_2017;




DROP TABLE IF EXISTS scot_data.drug_deaths_staged;
CREATE TABLE scot_data.drug_deaths_staged AS
    SELECT
        A.heroin_morphine_deaths_value,
        A.all_drug_related_deaths,
        A.calendar_date,
        A.area_name AS la_name,
        COALESCE(B.la_code, 'Scotland') AS la_code
    FROM (
        SELECT
            heroin_morphine_deaths_value,
            all_drug_related_deaths,
            calendar_date,
            CASE
                WHEN area_name = 'Argyll & Bute'       THEN 'Argyll and Bute'
                WHEN area_name = 'Dumfries & Galloway' THEN 'Dumfries and Galloway'
                WHEN area_name = 'Edinburgh, City of'  THEN 'City of Edinburgh'
                WHEN area_name = 'Eilean Siar'         THEN 'Na h-Eileanan Siar'
                WHEN area_name = 'Perth & Kinross'     THEN 'Perth and Kinross'
                ELSE area_name
            END AS area_name
        FROM scot_data.drug_deaths_raw_all_years
    ) A
    LEFT JOIN scot_data.la_code_name_lookup_staged B
    ON A.area_name = B.la_name;







-- PLAY:

------------------
-- TOTAL DEATHS --
------------------

-- Example: Aberdeen City:
select
      *
  from (
  select all_drug_related_deaths,
      all_drug_related_deaths - lead(all_drug_related_deaths) over (partition by la_code order by calendar_date) as after_1_year_diff,
      all_drug_related_deaths - lead(all_drug_related_deaths, 3) over (partition by la_code order by calendar_date) as after_3_year_diff,
      all_drug_related_deaths - lead(all_drug_related_deaths, 6) over (partition by la_code order by calendar_date) as after_6_year_diff,
      la_name, calendar_date
  from scot_data.drug_deaths_staged
  WHERE calendar_date >= '2008-01-01'
  ) a
  where calendar_date = '2008-01-01' order by after_6_year_diff desc;

--+---------------------------+---------------------+---------------------+---------------------+-----------------------+-----------------+
--| all_drug_related_deaths   | after_1_year_diff   | after_3_year_diff   | after_6_year_diff   | la_name               | calendar_date   |
--|---------------------------+---------------------+---------------------+---------------------+-----------------------+-----------------|
--| 121                       | -14                 | 4                   | 7                   | Glasgow City          | 2008-01-01      |
--| 16                        | 11                  | 11                  | 7                   | Perth and Kinross     | 2008-01-01      |
--| 23                        | 10                  | 6                   | 4                   | West Dunbartonshire   | 2008-01-01      |
--| 20                        | 6                   | -1                  | 3                   | Highland              | 2008-01-01      |
--| 11                        | -7                  | -8                  | 3                   | Aberdeenshire         | 2008-01-01      |
--| 6                         | 1                   | 4                   | 2                   | East Dunbartonshire   | 2008-01-01      |
--| 3                         | 1                   | 2                   | 2                   | Na h-Eileanan Siar    | 2008-01-01      |
--| 12                        | 4                   | -2                  | 1                   | South Ayrshire        | 2008-01-01      |
--| 6                         | -1                  | 3                   | 1                   | East Renfrewshire     | 2008-01-01      |
--| 3                         | -4                  | -7                  | 1                   | Moray                 | 2008-01-01      |
--| 1                         | 1                   | 1                   | 1                   | Orkney Islands        | 2008-01-01      |
--| 27                        | 0                   | -2                  | 1                   | Aberdeen City         | 2008-01-01      |
--| 10                        | 5                   | -1                  | 1                   | Falkirk               | 2008-01-01      |
--| 15                        | -4                  | -1                  | 0                   | North Ayrshire        | 2008-01-01      |
--| 8                         | -1                  | 0                   | 0                   | Angus                 | 2008-01-01      |
--| 9                         | 3                   | 0                   | -1                  | Stirling              | 2008-01-01      |
--| 6                         | -3                  | 2                   | -1                  | Midlothian            | 2008-01-01      |
--| 15                        | -6                  | 2                   | -1                  | West Lothian          | 2008-01-01      |
--| 4                         | 1                   | -2                  | -2                  | Clackmannanshire      | 2008-01-01      |
--| 29                        | -1                  | -3                  | -2                  | Dundee City           | 2008-01-01      |
--| 1                         | 1                   | -2                  | -3                  | Shetland Islands      | 2008-01-01      |
--| 30                        | -5                  | 3                   | -3                  | North Lanarkshire     | 2008-01-01      |
--| 27                        | 1                   | 3                   | -3                  | Renfrewshire          | 2008-01-01      |
--| 4                         | -3                  | -8                  | -4                  | Argyll and Bute       | 2008-01-01      |
--| 7                         | 2                   | -1                  | -4                  | Scottish Borders      | 2008-01-01      |
--| 9                         | 1                   | -3                  | -4                  | Dumfries and Galloway | 2008-01-01      |
--| 7                         | 1                   | -1                  | -4                  | East Lothian          | 2008-01-01      |
--| 13                        | 1                   | -4                  | -4                  | East Ayrshire         | 2008-01-01      |
--| 66                        | 21                  | 18                  | -5                  | City of Edinburgh     | 2008-01-01      |
--| 37                        | 5                   | 3                   | -9                  | Fife                  | 2008-01-01      |
--| 23                        | 4                   | -11                 | -11                 | South Lanarkshire     | 2008-01-01      |
--| 5                         | -2                  | -15                 | -12                 | Inverclyde            | 2008-01-01      |
--+---------------------------+---------------------+---------------------+---------------------+-----------------------+-----------------+





-- Of the above, 1 local authority from the ONS descriptors was picked to be shown against the scottiash avg.

S12000033 Aberdeen 'City Larger Towns and Cities'
S12000041 Angus 'Scottish Countryside'
S12000005 Clackmannanshire 'Services and Industrial Legacy'




select
       *
   from (
   select all_drug_related_deaths,
       all_drug_related_deaths - lead(all_drug_related_deaths) over (partition by la_code order by calendar_date) as after_1_year_diff,
       all_drug_related_deaths - lead(all_drug_related_deaths, 3) over (partition by la_code order by calendar_date) as after_3_year_diff,
       all_drug_related_deaths - lead(all_drug_related_deaths, 6) over (partition by la_code order by calendar_date) as after_6_year_diff,
       la_name, calendar_date
   from scot_data.drug_deaths_staged
   WHERE calendar_date >= '2008-01-01' AND la_name IN ('Aberdeen City', 'Angus', 'Clackmannanshire', 'Scotland')
   ) a
   where calendar_date = '2008-01-01' order by after_6_year_diff desc;
--+---------------------------+---------------------+---------------------+---------------------+------------------+-----------------+
--| all_drug_related_deaths   | after_1_year_diff   | after_3_year_diff   | after_6_year_diff   | la_name          | calendar_date   |
--|---------------------------+---------------------+---------------------+---------------------+------------------+-----------------|
--| 27                        | 0                   | -2                  | 1                   | Aberdeen City    | 2008-01-01      |
--| 8                         | -1                  | 0                   | 0                   | Angus            | 2008-01-01      |
--| 4                         | 1                   | -2                  | -2                  | Clackmannanshire | 2008-01-01      |
--+---------------------------+---------------------+---------------------+---------------------+------------------+-----------------+





create table scot_data.table_drug_deaths_timeseries_3_codes_and_scotland as
select
   la_name,
   calendar_date,
   all_drug_related_deaths
from scot_data.drug_deaths_staged
WHERE calendar_date >= '2007-01-01' AND la_name IN ('Aberdeen City', 'Angus', 'Clackmannanshire')

union all

select
    'Scotland AVG' as la_name,
   calendar_date,
   avg(all_drug_related_deaths)::int AS avg_scotland_deaths
from scot_data.drug_deaths_staged
where calendar_date >= '2007-01-01' and la_name != 'Scotland'
group by
    calendar_date;

\copy scot_data.table_drug_deaths_timeseries_3_codes_and_scotland TO '/Users/rupert/hannah_data/drug_deaths/tables/table_drug_deaths_timeseries_3_codes_and_scotland.csv' WITH DELIMITER ',' CSV HEADER;











create table scot_data.table_drug_deaths_timeseries_all_codes as
select
   la_name,
   calendar_date,
   all_drug_related_deaths
from scot_data.drug_deaths_staged
WHERE calendar_date >= '2007-01-01' AND la_name != 'Scotland'





















-- Worst increase over 9 years from 2008:

select
    *
from (
select
    heroin_morphine_deaths_value - lead(heroin_morphine_deaths_value) over (partition by la_code order by calendar_date desc) as after_1_year_diff,
    heroin_morphine_deaths_value - lead(heroin_morphine_deaths_value, 3) over (partition by la_code order by calendar_date desc) as after_3_year_diff,
    heroin_morphine_deaths_value - lead(heroin_morphine_deaths_value, 6) over (partition by la_code order by calendar_date desc) as after_6_year_diff,
    la_name
from scot_data.drug_deaths_staged
where calendar_date in ('2008-01-01', '2016-01-01') and la_code = ''
) a
where diff is not null
order by diff desc

+--------+-----------------------+
|Diff in |
| deaths |
| 2008-  |
| to     |
| 2017   | la_name               |
|--------+-----------------------|
| 24     | Glasgow City          |
| 20     | Dundee City           |
| 19     | Fife                  |
| 13     | Aberdeen City         |
| 13     | City of Edinburgh     |
| 10     | Dumfries and Galloway |
| 8      | North Lanarkshire     |
| 7      | Angus                 |
| 7      | Inverclyde            |
| 6      | East Lothian          |
| 6      | Scottish Borders      |
| 6      | South Lanarkshire     |
| 5      | West Lothian          |
| 5      | North Ayrshire        |
| 4      | East Ayrshire         |
| 3      | Falkirk               |
| 2      | Midlothian            |
| 2      | Aberdeenshire         |
| 2      | Moray                 |
| 1      | Shetland Islands      |
| 1      | Highland              |
| 1      | Orkney Islands        |
| 1      | Renfrewshire          |
| 1      | South Ayrshire        |
| 0      | Perth and Kinross     |
| 0      | West Dunbartonshire   |
| 0      | East Dunbartonshire   |
| -1     | Stirling              |
| -1     | Clackmannanshire      |
| -1     | Na h-Eileanan Siar    |
| -1     | Argyll and Bute       |
| -2     | East Renfrewshire     |
+--------+-----------------------+



-- for 2008 and 2017:

+--------+-----------------------+
| diff   | la_name               |
|--------+-----------------------|
| 25     | City of Edinburgh     |
| 20     | South Lanarkshire     |
| 14     | Glasgow City          |
| 12     | North Lanarkshire     |
| 11     | North Ayrshire        |
| 10     | South Ayrshire        |
| 9      | East Ayrshire         |
| 9      | Renfrewshire          |
| 9      | Falkirk               |
| 8      | Angus                 |
| 7      | Aberdeen City         |
| 7      | Fife                  |
| 6      | East Lothian          |
| 6      | West Lothian          |
| 6      | Inverclyde            |
| 5      | Clackmannanshire      |
| 5      | Dumfries and Galloway |
| 3      | Scottish Borders      |
| 3      | Moray                 |
| 2      | East Dunbartonshire   |
| 1      | Orkney Islands        |
| 0      | Argyll and Bute       |
| 0      | Aberdeenshire         |
| 0      | Shetland Islands      |
| -1     | Na h-Eileanan Siar    |
| -1     | East Renfrewshire     |
| -1     | Dundee City           |
| -2     | Midlothian            |
| -3     | Perth and Kinross     |
| -3     | West Dunbartonshire   |
| -3     | Stirling              |
| -5     | Highland              |
+--------+-----------------------+









-- Diff between 2008 and 1, 3 and 6 years after:

select heroin_morphine_deaths_value,
     lead(heroin_morphine_deaths_value) over (partition by la_code order by calendar_date) - heroin_morphine_deaths_value as after_1_year_diff,
     lead(heroin_morphine_deaths_value, 3) over (partition by la_code order by calendar_date) - heroin_morphine_deaths_value as after_3_year_diff,
     lead(heroin_morphine_deaths_value, 6) over (partition by la_code order by calendar_date) - heroin_morphine_deaths_value as after_6_year_diff,
     la_name, la_code, calendar_date
 from scot_data.drug_deaths_staged
 where la_code = 'S12000036'

-- Example for edinburgh:
+--------------------------------+---------------------+---------------------+---------------------+-------------------+-----------+-----------------+
| heroin_morphine_deaths_value   | after_1_year_diff   | after_3_year_diff   | after_6_year_diff   | la_name           | la_code   | calendar_date   |
|--------------------------------+---------------------+---------------------+---------------------+-------------------+-----------+-----------------|
| 17                             | 4                   | 2                   | 8                   | City of Edinburgh | S12000036 | 2007-01-01      |
| 21                             | -3                  | -11                 | 8                   | City of Edinburgh | S12000036 | 2008-01-01      |
| 18                             | 1                   | -6                  | 5                   | City of Edinburgh | S12000036 | 2009-01-01      |
| 19                             | -9                  | 6                   | 27                  | City of Edinburgh | S12000036 | 2010-01-01      |
| 10                             | 2                   | 19                  | 24                  | City of Edinburgh | S12000036 | 2011-01-01      |
| 12                             | 13                  | 11                  | <null>              | City of Edinburgh | S12000036 | 2012-01-01      |
| 25                             | 4                   | 21                  | <null>              | City of Edinburgh | S12000036 | 2013-01-01      |
| 29                             | -6                  | 5                   | <null>              | City of Edinburgh | S12000036 | 2014-01-01      |
| 23                             | 23                  | <null>              | <null>              | City of Edinburgh | S12000036 | 2015-01-01      |
| 46                             | -12                 | <null>              | <null>              | City of Edinburgh | S12000036 | 2016-01-01      |
| 34                             | <null>              | <null>              | <null>              | City of Edinburgh | S12000036 | 2017-01-01      |
+--------------------------------+---------------------+---------------------+---------------------+-------------------+-----------+-----------------+












-- Each area value against average for scotland:
select
    heroin_morphine_deaths_value,
    avg(heroin_morphine_deaths_value) over(partition by calendar_date)::int as scottish_council_avg,
    la_name,
    calendar_date
from scot_data.drug_deaths_staged
order by calendar_date, la_name;


-- This data is bigger and needs putting into excel.










-- Yearly change against scottish avg:


select
    yearly_change,

from (
select
    heroin_morphine_deaths_value - lead(heroin_morphine_deaths_value) over (partition by la_code order by calendar_date desc) as yearly_change,
    avg(heroin_morphine_deaths_value) over() as scottish_council_avg,
    la_name
from scot_data.drug_deaths_staged
) a
where diff is not null
order by diff desc;





















