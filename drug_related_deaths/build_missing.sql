-- This extra file enriches the other drug related death data, by adding the missing values for total drug deaths by council area
-- for the years 1997-2010.


DROP TABLE IF EXISTS scot_data.drug_deaths_missing_1997_2010;
CREATE TABLE scot_data.drug_deaths_missing_1997_2010 (
    council_area text,
    value_2000 text,
    value_2001 text,
    value_2002 text,
    value_2003 text,
    value_2004 text,
    value_2005 text,
    value_2006 text,
    value_2007 text,
    value_2008 text,
    value_2009 text,
    value_2010 text,
    unknown_1 text,
    avg_1996_to_2000 text,
    avg_2006_to_2010 text,
    unknown_2 text,
    population_in_2008 text,
    average_deaths_per_1000_pop text
);

\copy scot_data.drug_deaths_missing_1997_2010 FROM '/Users/rupert/hannah_data/drug_deaths/raw/total_drug_deaths_2010_council.csv' WITH DELIMITER ',' CSV HEADER;





DROP TABLE IF EXISTS scot_data.drug_deaths_missing_1997_2010;
CREATE TABLE scot_data.drug_deaths_missing_1997_2010 AS
    SELECT
        value_2000 text,
        value_2001 text,
        value_2002 text,
        value_2003 text,
        value_2004 text,
        value_2005 text,
        value_2006 text,
        value_2007 text,
        value_2008 text,
        value_2009 text,
        value_2010 text,
    FROM
    WHERE council_area IS NOT NULL;







