import os
import pandas as pd


for file in os.listdir('/Users/rupert/hannah_data/'):
    print(file)
    if file == '2017.csv':
        myHeader=8
        skipFooter=15
    elif file == '2016.csv':
        myHeader=8
        skipFooter=11
    elif file == '2015.csv':
        myHeader=8
        skipFooter=12
    elif file == '2014.csv':
        myHeader=8
        skipFooter=15
    elif file == '2013.csv':
        myHeader=9
        skipFooter=15
    elif file == '2012.csv':
        myHeader=8
        skipFooter=9
    elif file == '2011.csv':
        myHeader=6
        skipFooter=11
    df = pd.read_csv(f'/Users/rupert/hannah_data/{file}', encoding='latin8', header=myHeader, skipfooter=skipFooter, engine='python')
    df = df[pd.notnull(df.iloc[:,0])]
    # df = df.rename(columns={'Unnamed: 9': 'Benzodiazepines: Diazepam'})