CREATE TABLE scot_data.new_clients_at_drug_treatment_centres_staged AS
    SELECT
        TO_DATE(REGEXP_REPLACE(TRIM(date_code), '\/[\d]{4}', ''), 'YYYY') AS calendar_date,
        TRIM(feature_code) AS area_code,

    FROM scot_data.new_clients_at_drug_treatment_centres_raw
    WHERE