CREATE OR REPLACE FUNCTION quarter_to_date(text)
   returns date language sql as $$
       SELECT
       CASE
           WHEN REGEXP_REPLACE(TRIM($1), '[\d]{4}-', '') = 'Q1' THEN TO_DATE(CONCAT(REGEXP_REPLACE(TRIM($1), '\-Q\d', ''), '-', '03'), 'YYYY-MM')
           WHEN REGEXP_REPLACE(TRIM($1), '[\d]{4}-', '') = 'Q2' THEN TO_DATE(CONCAT(REGEXP_REPLACE(TRIM($1), '\-Q\d', ''), '-', '06'), 'YYYY-MM')
           WHEN REGEXP_REPLACE(TRIM($1), '[\d]{4}-', '') = 'Q3' THEN TO_DATE(CONCAT(REGEXP_REPLACE(TRIM($1), '\-Q\d', ''), '-', '09'), 'YYYY-MM')
           WHEN REGEXP_REPLACE(TRIM($1), '[\d]{4}-', '') = 'Q4' THEN TO_DATE(CONCAT(REGEXP_REPLACE(TRIM($1), '\-Q\d', ''), '-', '12'), 'YYYY-MM')
       END
   $$;