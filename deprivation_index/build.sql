CREATE TABLE scot_data.deprivation_raw (
    Data_Zone text,
    Intermediate_Zone text,
    Council_area text,
    Total_population text,
    Working_age_population_revised text,
    Overall_SIMD16_rank text,
    Income_domain_2016_rank text,
    Employment_domain_2016_rank text,
    Health_domain_2016_rank text,
    Education_domain_2016_rank text,
    Housing_domain_2016_rank text,
    Access_domain_2016_rank text,
    Crime_domain_2016_rank text
);




\copy scot_data.deprivation_raw FROM '/Users/rupert/Downloads/deprivation.csv' WITH DELIMITER ',' CSV HEADER;




CREATE TABLE scot_data.deprivation_staged AS
    SELECT
        TRIM(council_area) AS council_area,
        AVG(REPLACE(overall_simd16_rank, ',', '')::int) AS simd_rank
    FROM scot_data.deprivation_raw
    GROUP BY 1;









