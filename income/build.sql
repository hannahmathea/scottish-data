


CREATE TABLE scot_data.income_staged AS
    SELECT
        TO_DATE(TRIM(date_code), 'YYYY') AS calendar_date,
        TRIM(feature_code) AS area_code,
        TRIM(gender) AS gender,
        value::double precision AS income_value
    FROM scot_data.income_raw
    WHERE measurement = 'Median' and working_pattern = 'All Patterns' AND gender IN ('All');




DROP TABLE IF EXISTS scot_data.income_staged;
CREATE TABLE scot_data.income_staged AS
    SELECT
        A.calendar_date,
        A.la_code,
        A.gender,
        A.income_value,
        B.la_name
    FROM (
        SELECT
            TO_DATE(TRIM(date_code), 'YYYY') AS calendar_date,
            TRIM(feature_code) AS la_code,
            TRIM(gender) AS gender,
            AVG(value::double precision) AS income_value
        FROM scot_data.income_raw
        WHERE measurement = 'Median' and working_pattern = 'All Patterns' AND gender = 'All' and date_code >= '2007' and date_code <= '2017'
        GROUP BY 1, 2, 3
    ) A
    JOIN scot_data.la_code_name_lookup_staged B
    ON A.la_code = B.la_code;








-- PLAY:

-- Table 1. Income for our 3 areas and scotland AVG.

DROP TABLE IF EXISTS scot_data.table_income_timeseries_3_codes_and_scotland;
CREATE TABLE scot_data.table_income_timeseries_3_codes_and_scotland AS
    SELECT
        la_name,
        calendar_date,
        income_value
    FROM scot_data.income_staged
    WHERE la_name IN ('Aberdeen City', 'Angus', 'Clackmannanshire')
    UNION ALL
    SELECT
        'Scotland AVG' AS la_name,
        calendar_date,
        AVG(income_value)::int
    FROM scot_data.income_staged
        GROUP BY calendar_date;




























