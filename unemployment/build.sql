DROP TABLE IF EXISTS scot_data.unemployment_staged;
CREATE TABLE scot_data.unemployment_staged AS
    SELECT
        QUARTER_TO_DATE(A.date_code) AS calendar_date,
        TRIM(A.feature_code) AS la_code,
        A.value AS population_value,
        B.la_name
    FROM scot_data.unemployment_raw A
    JOIN scot_data.la_code_name_lookup_staged B
    ON TRIM(A.feature_code) = B.la_code AND measurement = 'Count';




DROP TABLE IF EXISTS scot_data.unemployment_staged_la_level_yearly;
CREATE TABLE scot_data.unemployment_staged_la_level_yearly AS
    SELECT
        la_code,
        la_name,
        TO_DATE(TO_CHAR(calendar_date::date, 'YYYY'), 'YYYY-MM-dd') AS calendar_date,
        AVG(population_value::int)::int AS population_value
    FROM scot_data.unemployment_staged
    GROUP BY la_code, la_name, TO_DATE(TO_CHAR(calendar_date::date, 'YYYY'), 'YYYY-MM-dd');




DROP TABLE IF EXISTS scot_data.table_unemployment_timeseries_3_codes_and_scotland;
CREATE TABLE scot_data.table_unemployment_timeseries_3_codes_and_scotland AS
    SELECT
        la_name,
        calendar_date,
        population_value::int AS unemployment_value
    FROM scot_data.unemployment_staged_la_level_yearly
    WHERE calendar_date >= '2007-01-01' AND la_name IN ('Aberdeen City', 'Angus', 'Clackmannanshire')
    UNION ALL
    SELECT
        'Scotland',
        calendar_date,
        AVG(population_value)::int AS population_value
    FROM scot_data.unemployment_staged_la_level_yearly
    WHERE calendar_date >= '2007-01-01'
    GROUP BY calendar_date;











CREATE TABLE scot_data.table_unemployment_timeseries_all_codes AS
    SELECT
        la_name,
        calendar_date,
        population_value::int AS unemployment_value
    FROM scot_data.unemployment_staged_la_level_yearly
    WHERE calendar_date >= '2007-01-01';














