CREATE TABLE scot_data.table_unemployment_regression_master AS
    SELECT
        A.la_name,
        A.calendar_date,
        A.unemployment_value/C.population_value::double precision*100 AS unemployment_as_perc_of_pop,
        B.all_drug_related_deaths/C.population_value::double precision*100 AS drug_deaths_as_perc_of_pop
    FROM scot_data.table_unemployment_timeseries_all_codes A
    JOIN scot_data.table_drug_deaths_timeseries_all_codes B
    ON A.la_name = B.la_name AND A.calendar_date = B.calendar_date
    JOIN scot_data.population_staged C
    ON A.la_name = C.la_name AND A.calendar_date = C.calendar_date;









CREATE TABLE scot_data.table_income_regression_master AS
    SELECT
        A.la_name,
        A.calendar_date,
        A.income_value,
        B.all_drug_related_deaths/C.population_value::double precision*100 AS drug_deaths_as_perc_of_pop
    FROM scot_data.income_staged A
    JOIN scot_data.table_drug_deaths_timeseries_all_codes B
    ON A.la_name = B.la_name AND A.calendar_date = B.calendar_date
    JOIN scot_data.population_staged C
    ON A.la_name = C.la_name AND A.calendar_date = C.calendar_date;














