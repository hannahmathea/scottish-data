-- Source: <https://geoportal.statistics.gov.uk/datasets/17eb563791b648f9a7025ca408bb09c6_0>


CREATE TABLE scot_data.la_code_name_lookup_staged AS
    SELECT
        TRIM(lad18cd) AS la_code,
        TRIM(lad18nm) AS la_name,
        lad18nmw,
        fid
    FROM scot_data.la_code_name_lookup;