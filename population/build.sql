CREATE TABLE scot_data.population_staged AS
    SELECT
        la_name,
        year_2007 AS population_value,
        '2007-01-01'::date AS calendar_date
    FROM scot_data.population
    UNION ALL
    SELECT
        la_name,
        year_2007 AS population_value,
        '2008-01-01'::date AS calendar_date
    FROM scot_data.population
    UNION ALL
    SELECT
        la_name,
        year_2007 AS population_value,
        '2009-01-01'::date AS calendar_date
    FROM scot_data.population
    UNION ALL
    SELECT
        la_name,
        year_2007 AS population_value,
        '2010-01-01'::date AS calendar_date
    FROM scot_data.population
    UNION ALL
    SELECT
        la_name,
        year_2011 AS population_value,
        '2011-01-01'::date AS calendar_date
    FROM scot_data.population
    UNION ALL
    SELECT
        la_name,
        year_2012 AS population_value,
        '2012-01-01'::date AS calendar_date
    FROM scot_data.population
    UNION ALL
    SELECT
        la_name,
        year_2013 AS population_value,
        '2013-01-01'::date AS calendar_date
    FROM scot_data.population
    UNION ALL
    SELECT
        la_name,
        year_2014 AS population_value,
        '2014-01-01'::date AS calendar_date
    FROM scot_data.population
    UNION ALL
    SELECT
        la_name,
        year_2015 AS population_value,
        '2015-01-01'::date AS calendar_date
    FROM scot_data.population
    UNION ALL
    SELECT
        la_name,
        year_2016 AS population_value,
        '2016-01-01'::date AS calendar_date
    FROM scot_data.population
    UNION ALL
    SELECT
        la_name,
        year_2017 AS population_value,
        '2017-01-01'::date AS calendar_date
    FROM scot_data.population;